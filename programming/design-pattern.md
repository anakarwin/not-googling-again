# Design Pattern for Human
Design pattern for human actually is described beautifully in [this repository](https://github.com/kamranahmedse/design-patterns-for-humans). My knowledge is for summarize what have i learn in several sentences.

- [Creational](#creational)

    Simple Factory | Factory Method | Abstract Method | Builder | Prototype (Cloning) | Singleton

- [Structural](#structural)

    Adapter | Bridge | Composite | Decorator | Facade | Flyweight | Proxy

- [Behavioral](#behavioral)

    Chain of Responsibility | Command | Iterator | Mediator | Memento | Observer | Visitor | Strategy | State | Template Method

## Creational
- **Simple Factory**

    Factory that create child class like ShapeFactory that create Circle, Square, Triangle.

- **Factory Method**
  
  Factory that encapsulate different child's method with same parent's method. For example, `Manager.takeInterview()` encapsulate `EngineeringManager.codeTest()` and `UserManager.cultureCheck()`

- **Abstract Method**

    Factory of factories, make child that need strict dependencies

- **Builder**

    Avoid too many arguments in constructor

- **Prototype (Cloning**)

    Clone another object, different from javascript's prototype

- **Singleton**

    The only one object in his class


## Structural

- **Adapter**

    Adapting one class to another class
    ![Improvise-Adapt-Overcome](adapter.jpg)

- **Bridge**

    Composition over inheritance, bridge of two Parent class hierarchy.

- **Composite**

    Treat polymophism in same way, in other hand, two class implement same interface.

- **Decorator**

    Decorate parent class as new child class. In Python, decorator can be done in function level (High Order Function).

- **Facade**

    Do more things in one method

- **Flyweight**

    Memoize same thing

- **Proxy**

    Do everything via 3<sup>rd</sup> (middle) class


## Behavioral

- **Chain of Responsibility**

    Check item in chain until valid like linked list.

- **Command**

    Execute transaction in the end and enable to undo-redo

- **Iterator**

    Iterate through item. Most programming language have this.

- **Mediator**

    Control interaction between two class

- **Memento**

    Enable to save/load state

- **Observer**

    The Observable object call all Observer objects (listener).

- **Visitor**

    Add operation without changing the structure of the class

- **Strategy**

    Changable behaviour of the class. For example, Sorting algorithm, we can change the algorithm behind the `sort()` function.

- **State**

    Class with `set_state()` method. React users will be familiar with this

- **Template Method**

    Do some step we don't must know. May be like Facade