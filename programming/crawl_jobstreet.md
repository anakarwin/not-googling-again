# Crawl Jobstreet
Crawl Jobstreet salary in browser using browser's console (using `javascript`).
The code should be run while logged in on Jobstreet site.
The code is example use of delaying loop of the `fetch` function using `setTimeout` function.

The result of crawling will be stored in `result` variable. Result can be shown by run `document.write(JSON.stringify(result));`.

```javascript
var crawling = function(pg, result) {
	fetch('https://www.jobstreet.co.id/id/job-search/job-vacancy.php?area=1&option=1&specialization=191&job-source=1%2C64&classified=0&job-posted=0&sort=1&order=0&src=16&srcr=12&ojs=3&pg=' + this.pg, {
		method: 'GET',  credentials: 'same-origin'
	})
	.then(res => res.text())
	.then(html => {
		console.log('Processing pg:' + this.pg);
		var parser = new DOMParser();
		var doc = parser.parseFromString(html, "text/html");
		var root = doc.querySelector('#job_listing_panel');
		if (!root) {
			console.log('root not found');
			console.log(doc);
		} else {
			for (var i=0, n=root.children.length; i<n; i++) {
				var child = root.children[i];
				var clsNode = child.getAttribute('class');
				var cls = clsNode ? clsNode.trim() : '';
				if (cls == 'panel') {
					var companyNode = child.querySelector('.company-name');
					var company = companyNode ? companyNode.textContent.trim() : '';
					var locationNode = child.querySelector('.job-location');
					var location = locationNode ? locationNode.textContent.trim() : '';
					var salaryNode = child.querySelector('.under-expected-salary');
					var salary = salaryNode ? salaryNode.textContent.trim() : '';
					var linkNode = child.querySelector('.position-title-link')
					var link = linkNode ? linkNode.getAttribute('href') : '';
					var positionNode = child.querySelector('.position-title-link');
					var position = positionNode ? positionNode.textContent.trim() : '';
					this.result.push({
						'company': company, 'location': location,
						'salary': salary, 'link': link,
						'position': position
					});
				}
			}
			setTimeout(crawling.bind({pg: this.pg + 1, result: this.result}), 5000);
		}
	})
}

var result = [];
setTimeout(crawling.bind({pg: 1, result: result}), 1000);
```