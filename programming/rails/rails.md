# All about Ruby on Rails

All about Ruby on Rails that should not googling again is stored here.

# Best Practices
## ERB Comments
For single line comment, use `<%#= comment %>`. For multi line comment, use following snippet
```erb
<%
=begin %>
Multilines comment
<%
=end %>
```

# Tutorials
This tutorial is for rails version 4.7.2
See [Tutorials](tutorial.md)