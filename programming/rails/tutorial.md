# Ruby on Rails Tutorial
Based on Rails version 4.7.2

- [Ruby on Rails Tutorial](#ruby-on-rails-tutorial)
  - [Create New Project](#create-new-project)
  - [Run server](#run-server)
  - [Add Controller](#add-controller)
    - [Removing Generated Controller](#removing-generated-controller)
    - [Set the Views](#set-the-views)
  - [Set Views as Root](#set-views-as-root)
  - [REST-ful API CRUD](#rest-ful-api-crud)
    - [Magic of Template](#magic-of-template)
    - [Form - Add Resource Instance](#form---add-resource-instance)
    - [Controller - Creating Resource Instance](#controller---creating-resource-instance)
  - [Creating Model](#creating-model)
  - [Saving Data in the Controller](#saving-data-in-the-controller)
  - [Listing All Resources (include Links)](#listing-all-resources-include-links)
  - [Model Validation](#model-validation)
    - [Update New Form](#update-new-form)
  - [Updating Articles](#updating-articles)
    - [View Templating](#view-templating)
  - [Deleting Articles](#deleting-articles)
  - [Adding Comments](#adding-comments)

## Create New Project
```bash
rails new <app_name>
```

## Run server
```bash
rails server
```

## Add Controller
```bash
rails generate controller <ControllerName> [view_name]
```
`[view_name]` to generate `views` in `app/views/<controllerName>/<view_name>.html.erb` and register route in `config/routes.rb`

### Removing Generated Controller
```bash
rails destroy controller <ControllerName> [view_name]
```

### Set the Views
Change the view page on `app/views/<controller_name>/<view_name>.html.erb`

## Set Views as Root
Modify file `config/routes.rb` by adding this line
```ruby
root <ControllerName>#<methodName>
```

## REST-ful API CRUD
- Add this line in file `config/routes.rb` above the `root` declaration
  ```ruby
  resources :<resourceName>
  ```
- Tell rails to generate resource path. (on newer version, using `rails` `routes` command)
  ```bash
  rake routes
  ```
  On newer version, the command can be executed from `rails`
  ```bash
  rails routes
  ```
- Generate the corresponding controller
  ```bash
  rails generate controller <ResourceName>
  ```
- Add the REST methods in generated Controller (`new` method)

### Magic of Template
- Each method in `app/controllers/<resource_name>_controller.rb` can be called as `REST` path `/<resource_name>/<method_name>`

    For create new instance, the method is `new`
- View template should be defined in `app/views/<resource_name>/<method_name>.html.erb`
- Implement `<method>` in the controller

### Form - Add Resource Instance
[Version 4.2.7] Form generated using `form_for` function
```erb
<%= form_for :<resource_instance>, url: <resource_name>_path do |f| %>
  <p>
    <%= f.label :title %><br>
    <%= f.text_field :title %>
  </p>
 
  <p>
    <%= f.label :text %><br>
    <%= f.text_area :text %>
  </p>
 
  <p>
    <%= f.submit %>
  </p>
<% end %>
```

[Version 5.2] Form can be generated using `form_with` function with parameter `scope:`
```erb
<%= form_with scope: :<resource_instance>, url: <resource_name>_path, local: true do |form| %>
    <p>
        <%= form.label :title %>
        <%= form.text_field :title %>
    </p>

    <p>
        <%= form.label :text %>
        <%= form.text_area :text %>
    </p>

    <p>
        <%= form.submit %>
    </p>
<% end %>
```
`resource_instance` means singular term of `resource_name`. For example if the `resources_name` is `articles`, so the `resoure_instance` is `article`.

### Controller - Creating Resource Instance
- Implement method `create` in corresponding `Controller`
  ```ruby
  def create
    render plain: params[:<resource_instance>].inspect
  end
  ```

## Creating Model
Creating the model (in this case `Article`) using this command
```bash
rails generate model Article title:string text:text
```
The command means create `Article` model with attribute `title` of type string and `text` of type text. This command will also create the migration in `app/db/migrate`. To do the migration, run the following command
```bash
rake db:migrate
```

## Saving Data in the Controller
Modify the `create` method in the controller class (in this case `ArticlesController`)
```ruby
def create
  @article = Article.new(params[:article])
 
  @article.save
  redirect_to @article
end
```
To avoid another parameters, use this line instead
```ruby
@article = Article.new(params.require(:article).permit(:title, :text))
```
The `redirect_to` function will redirect the page into Article page (`/articles/:id`) which requires implementation of method `show` in the contoller. Add following line inside the `show` method.
```ruby
@article = Article.find(params[:id])
```
And add the view for the `show` method in `app/views/articles/show.html/erb`.

## Listing All Resources (include Links)
The route for listing all Resources is `/<resource_name>` (in this case `/articles`) which requires `index` method. Implement the method by add the following line.
```ruby
@articles = Article.all
```
Also add the views and add the following lines.
```erb
<h1>Listing articles</h1>
<%= link_to 'My Blog', controller: 'articles' %>
 
<table>
  <tr>
    <th>Title</th>
    <th>Text</th>
  </tr>
 
  <% @articles.each do |article| %>
    <tr>
      <td><%= article.title %></td>
      <td><%= article.text %></td>
    </tr>
  <% end %>
</table>
```
The second line is function to navigate into another page. `controller` param is the **_magic_** key by using only the controller's name. Link can be replaced by one of these lines
```erb
<%= link_to 'My Blog', controller: 'welcome' %>
<%= link_to 'Home Page', welcome_index_path %>
<%= link_to 'New article', new_article_path %>
```
There are a lot of **_magics_**.

## Model Validation
Article model validation can be added in the `app/modes/article.rb` file by adding the following code.
```ruby
class Article < ActiveRecord::Base
  validates :title, presence: true,
                    length: { minimum: 5 }
end
```
The validation is using [Active Record Validations](https://guides.rubyonrails.org/v4.2/active_record_validations.html).

### Update New Form
Update new form page by adding the error message
```erb
<%= form_for :article, url: articles_path do |f| %>
 
  <% if @article.errors.any? %>
    <div id="error_explanation">
      <h2>
        <%= pluralize(@article.errors.count, "error") %> prohibited
        this article from being saved:
      </h2>
      <ul>
        <% @article.errors.full_messages.each do |msg| %>
          <li><%= msg %></li>
        <% end %>
      </ul>
    </div>
  <% end %>
  
  ...
<% end %>
```
The `@article.errors.any?` will throw `nilClass` error because the `@article` is not been instantiated. To prevent this, update the `new` method in the controller.
```ruby
def new
  @article = Article.new
end
```

## Updating Articles
While using the `rake routes` command, many routes have been generated. There are `edit` and `update` routes. The `edit` route is to show the edit page which is controlled by `edit` method and the `update` route is `PATCH` HTTP method and controlled by `update` method.
The following code is the code of `edit` and `update` method in contoller.
```ruby
def edit
    @article = Article.find(params[:id])
end

def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
        redirect_to @article
    else
        render 'edit'
    end
end
```
And the following code is the view of `edit` page which stored as `edit.html.erb`
```erb
<h1>Editing article</h1>
 
<%= form_for :article, url: article_path(@article), method: :patch do |f| %>
 
  <% if @article.errors.any? %>
    <div id="error_explanation">
      <h2>
        <%= pluralize(@article.errors.count, "error") %> prohibited
        this article from being saved:
      </h2>
      <ul>
        <% @article.errors.full_messages.each do |msg| %>
          <li><%= msg %></li>
        <% end %>
      </ul>
    </div>
  <% end %>
 
  <p>
    <%= f.label :title %><br>
    <%= f.text_field :title %>
  </p>
 
  <p>
    <%= f.label :text %><br>
    <%= f.text_area :text %>
  </p>
 
  <p>
    <%= f.submit %>
  </p>
 
<% end %>
 
<%= link_to 'Back', articles_path %>
```
The view is same as the `show` view. The different is only in the `url` and the `method` parameters. We can add `Show` and `Edit` link in the `index` page by adding these lines
```erb
    ...
    <td><%= link_to 'Show', article_path(article) %></td>
    <td><%= link_to 'Edit', edit_article_path(article) %></td>
    ...
```
Also add the `Edit` link in the `show` page by adding before the `Back` link
```erb
    ...
    <%= link_to 'Edit', edit_article_path(@article) %> |
    <%= link_to 'Back', articles_path %>
```

### View Templating
`Edit` and `New` page has same form. This duplicate can be removed by using view templating. Save the following form template into `app/views/articles/_form.html.erb`
```erb
<%= form_for @article do |f| %>
 
  <% if @article.errors.any? %>
    <div id="error_explanation">
      <h2>
        <%= pluralize(@article.errors.count, "error") %> prohibited
        this article from being saved:
      </h2>
      <ul>
        <% @article.errors.full_messages.each do |msg| %>
          <li><%= msg %></li>
        <% end %>
      </ul>
    </div>
  <% end %>
 
  <p>
    <%= f.label :title %><br>
    <%= f.text_field :title %>
  </p>
 
  <p>
    <%= f.label :text %><br>
    <%= f.text_area :text %>
  </p>
 
  <p>
    <%= f.submit %>
  </p>
 
<% end %>
```
Replace the `form` code from `edit` and `new` page by this line
```erb
<%= render 'form' %>
```
We don't need to define the `url` and the `method` anymore. This is another **_magic_**.

## Deleting Articles
Add `destroy` method to the controller and implement the following code
```ruby
def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
end
```
And don't forget to add the `Destroy` link on the home page
```erb
    ...
    <td><%= link_to 'Destroy', article_path(article), method: :delete, data: { confirm: 'Are you sure?' } %></td>
```
Deleting article will be prompted using `confirm` param.

## Adding Comments
Comment is a model that depend on Article model. To generate the model, run the following commands
```bash
rails generate model Comment commenter:string body:text article:references
rake db:migrate
```
Update the `Article` model that has many comments and will destroy all referencing `Comment`
```ruby
class Article < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    validates :title, presence: true, length: { minimum: 5 }
end
```
Edit the resource on `config/routes.rb` file
```ruby
  resources :articles do
    resources :comments
  end
```
Generate the controller by running the following command
```bash
rails generate controller Comments
```
Edit the `Article`'s `show` (`app/views/articles/show.html.erb`) view to display comment. 
```erb
<p>
  <strong>Title:</strong>
  <%= @article.title %>
</p>
 
<p>
  <strong>Text:</strong>
  <%= @article.text %>
</p>

<h2>Comments</h2>
<%= render @article.comments %>

<h2>Add a comment:</h2>
<%= render 'comments/form' %>

<%= link_to 'Edit', edit_article_path(@article) %> |
<%= link_to 'Back', articles_path %>
```
which using the `Comment`'s view in `app/views/comments/_comment.html.erb`
```erb
<p>
    <strong>Commenter:</strong>
    <%= comment.commenter %>
</p>

<p>
    <strong>Comment:</strong>
    <%= comment.body %>
</p>

<p>
    <%= link_to 'Edit Comment', edit_article_comment_path(@article, comment) %>
    <%= link_to 'Destroy Comment',
        [comment.article, comment],
        method: :delete,
        data: { confirm: 'Are you sure?' } %>
</p>
```
and using the `Comment`'s **form** view in `app/views/comments/_form.html.erb`
```erb
<%= form_for([@article, @article.comments.build]) do |f| %>
  <p>
    <%= f.label :commenter %><br>
    <%= f.text_field :commenter %>
  </p>
  <p>
    <%= f.label :body %><br>
    <%= f.text_area :body %>
  </p>
  <p>
    <%= f.submit %>
  </p>
<% end %>
```
Add required methods in Comment Controller
```ruby
class CommentsController < ApplicationController
    def create
        @article = Article.find(params[:article_id])
        @comment = @article.comments.create(comment_params)
        redirect_to article_path(@article)
    end

    def destroy
        @article = Article.find(params[:article_id])
        @comment = @article.comments.find(params[:id])
        @comment.destroy
        redirect_to article_path(@article)
    end

    def edit
        @article = Article.find(params[:article_id])
        @comment = @article.comments.find(params[:id])
    end

    # Returns JSON instead of views
    def index
        @article = Article.find(params[:article_id])
        @comments = @article.comments.all
        render json: @comments
    end

    # Returns JSON instead of views
    def show
        @article = Article.find(params[:article_id])
        @comment = @article.comments.find(params[:id])
        render json: @comment
    end

    def update
        @article = Article.find(params[:article_id])
        @comment = @article.comments.find(params[:id])
        if @comment.update(comment_params)
            redirect_to article_path(@article)
        else
            render 'edit_form'
        end
    end

    private
    def comment_params
        params.require(:comment).permit(:commenter, :body)
    end
end
```
Some methods return JSON for REST-ful API

If we see, editing comment is done on different form page called `edit_form` which stored in `app/views/comments/_edit_form.html.erb`
```erb
<%= form_for :comment, url: article_comment_path(@article, @comment), method: :patch do |f| %>
  <p>
    <%= f.label :commenter %><br>
    <%= f.text_field :commenter %>
  </p>
  <p>
    <%= f.label :body %><br>
    <%= f.text_area :body %>
  </p>
  <p>
    <%= f.submit %>
  </p>
<% end %>
```

