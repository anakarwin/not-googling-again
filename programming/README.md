# Programming
This section is all knowledge about programming.

# List of contents
- [Crawling Jobstreet](crawl_jobstreet.md)
- [Design Pattern for Human](design-pattern.md)
- [All about Python](python.md)
- [Ruby on Rails](rails/rails.md)