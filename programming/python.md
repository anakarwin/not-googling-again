# All about Python

All about Python that should not googling again is stored here.

- [All about Python](#all-about-python)
- [Quality Code](#quality-code)

# Quality Code

Quality code checking in Python using some packages, there are:
- `flake8` for code style checking. Common arguments are `--nax-complexity 10`
- `radon` to calculate Cyclomatic Complexity and Maintainability Index.
  Common arguments are `--min C` for `radon cc` and `--min B -s` for `radon mi`
- `vulture` to detect unused codes. Common arguments are `--min-confidence 100`
- `cohesion` to calculate cohesion in a module (Higher is better)
- `pypdeps` to create module dependency graph. Should install `graphviz` first
  and mmake sure `dot` is on environment