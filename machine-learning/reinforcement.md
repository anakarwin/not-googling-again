**Table of Contents**
- [Markov Decision Process](#markov-decision-process)
    - [Basic Formula](#basic-formula)
    - [Glossary](#glossary)
    - [Algorithms](#algorithms)
- [Temporal Difference Learning](#temporal-difference-learning)
    - [SARSA](#sarsa)
    - [Pseudo code: Q-Learning](#pseudo-code-q-learning)

# Markov Decision Process

## Basic Formula
MDP = {S, A, R, P, G}

S: States
A: Actions
R: Rewards
P: Transition Probability
G: Discount Factor

## Glossary
- V(s) -> State-value function
- Q(s,a) -> Action-value function
- Terminal state: State when episode ends

V(s) is *future* reward, so V(s) of Terminal state is 0

## Algorithms
- **Dynamic Programming**
- **Iterative Policy Equation**
- **Policy Iteration**
    - For solvinf control problem

        ```
        while not converged:
            policy evaluation on current policy
            policy improvement -> argmax Q(s,a)
        ```

- **Value Iteration**
- Monte Carlo
    - Only gives values for encountered state
        ```
        Initialized random policy
        while not converged:
            play an eposide
            return states
            policy improvement
        ```

# Temporal Difference Learning
- Monte Carlo: sample returns based on episode
- Temporal Difference: estimate returns based in current value function estimate
- look at TD(0)
- Spare memory inefficient
- Learn during the episode

## SARSA
  
  ```
  Q(s,a) <- Q(s,a) + alpha[r + G*Q(s',a') - Q(s,a)]
  a' = argmax of a {Q(s',a)}
  ```

## Pseudo code: Q-Learning
  
  ```
  Q(s,a) = arbitary
  Q(terminal,a) = 0
  for t = 1 .. N:
    s = start_state
    a = epsilon_greedy_from(Q(s))
    while not game over:
        s', r = do_action(a)
        a' = epsilon_greedy_from(Q(s'))
        Q(s,a) = Q(s,a) + alpha[r + G*Q(s',a') - Q(s,a)]
        s = s'
        a = a'
  ```