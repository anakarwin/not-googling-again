# Machine Learning - Artificial Intelligence
This section is all knowledge about Machine Learning or Artificial Intelligence.

# List of Contents
- [Anomaly Detection](anomaly.md)
- [Open CV](opencv.md)
- [Recommendation System](recsys.md)
- [Reinforcement Learning](reinforcement.md)