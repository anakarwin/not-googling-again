# Recommendation System

- [Recommendation System](#recommendation-system)
    - [Content-based](#content-based)
        - [Advantages](#advantages)
        - [Limitations](#limitations)
    - [Personalized Recommendation](#personalized-recommendation)
    - [Attribute-based](#attribute-based)
    - [Hierarchical Classification](#hierarchical-classification)
    - [Concept-based](#concept-based)
    - [Important Keywords](#important-keywords)
    - [Item Criteria: Collective Preference](#item-criteria-collective-preference)
    - [Item of Known Interest (IoKI)](#item-of-known-interest-ioki)
- [Algorithms](#algorithms)
    - [Memory Based](#memory-based)
    - [Model Based](#model-based)
    - [Hybrid](#hybrid)

## Content-based
### Advantages
- Cold-start
- No sparsity
- Recommend User's taste, new and unpopular items

### Limitations
- Structural data
- Unable use other user judgement

## Personalized Recommendation
- Association Rule Mining (Apriori)
- Statistic Measurement (Correlation, Condition, Dependence)

## Attribute-based
- user's attribute
- item's attribute

## Hierarchical Classification
- Categorization

    Categorize like domain hierarchy. Example:
    `Entertainment//Sports//MMA
    `

## Concept-based
- Supervised: Taxonomy / Dictionary
- Unsupervised: Document clustering

## Important Keywords
- Semantic
  
## Item Criteria: Collective Preference
- Number of item views
- Time on item view
- Number of item purchasing

## Item of Known Interest (IoKI)
- User cart activities
- Favorite / Wishlist
- Loan / Buy activities

# Algorithms
## Memory Based
- Correlation
- Cosine

## Model Based
- Association Rule
- Clustering
- Neural Network
- Decision Tree
- Link Analysis
- Regression
- Bayes Classifier
- Matrix Completion

## Hybrid
- Weighted Hybridization
- Switching
- Cascade
- Mixed
- Feature
- Combination
- Feature Augmentation
- Meta Level