# Anomaly Detection

## Supervised
- Neural Network
- SVM
- K-Nearest Neighbor
- Bayesian Network
- Decision Tree
- Regression

## Unsupervised
- Percentage based
- Different from normal
    - self-organize machine
    - K-means
    - C-means
    - Expecation Maximization
    - GMM (Gaussian Mixture Model)
    - Elliptical Covariance
    - OneClass SVM
    - Adaptive Resonance Theory
    - Kernel Density
- Nearest Neighbor
    - Local Outlier Factor
- Density based
    - GMM
    - KDE
- Kernel Approach
    - OneClass SVM
- Tree/Partitioning
    - Isolation Forest
  
## Time Series
- LSTM
- GRU
- HTM (Hierarchical Temporal Memory)

## Semi-Supervised
- Novelty detection

## Others
Measurement of Z-score:
- Standard Deviation
  
  ```
  Z = |xi - mean(x)|
      --------------
           sd(x)
  ```

- MAD (Median Absolute Deviation)

  ```
  Z = |xi - median(x)|
      ----------------
            MAD(x)

  MAD(x) = median(|xi - median(x)|)
  ```
  