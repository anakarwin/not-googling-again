# Open CV
All Open CV (mostly in Python) related knowledges.

- [Open CV](#open-cv)
    - [Books](#books)
    - [Codes](#codes)
    - [Color Space](#color-space)

## Books
Color FAQ: [ColorFAQ.pdf](http://poynton.ca/PDFs/ColorFAQ.pdf)

## Codes
Open CV Normalize:

```Python
cv2.normalize(
    img.astype('float'),
    None,
    0., 1.,
    cv2.NORM_MINMAX
)
```
## Color Space
- Gray (8 bit 1 channels uint)
- XYZ (3 channels floating point, Z may > 1)
- YCrCb (8-bit or floating point)
- HSV
    
    Value           |  8-bit  | float
    --------------- | ------- | ------
    0 <= H <= 360   | 0 - 180 | 0 - 1
    0 <= S, V <= 1  | 0 - 255 | 0 - 1

- HLS (Like HSV)
- LAB

    Value               | 8-bit
    ------------------- | -------
    0 <= L <= 100       | 0 - 255
    -127 <= a, b <= 127 | 0 - 255

- LUV

    Value               | 8-bit
    ------------------- | -------
    0 <= L <= 100       | 0 - 255
    -134 <= U <= 220    | 0 - 255
    -140 <= V <= 122    | 0 - 255